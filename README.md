# Projet TaRecette
Application pour enregistrer ses recettes personnelles et éviter la panne s'inspiration.

- Version python : 3.5
- Version django : 2.0


*Date de début : 30 mars 2018*

---

## Installation du projet

1. Créer **l'environnement virtuel** en utilisant virtualenv
	- `virtualenv venv -p /usr/bin/python3.5`
2. **Activer** l'environnement
	- `source venv/bin/activate`
3. Installer les **dépendances** (à venir)
	- `pip install django, python-slugify`
4. Installer les **migrations**
	- `./manage.py migrate`
5. Lancer un **serveur local**
    - `./manage.py runserver`

---

Après avoir installé nodejs et npm, installer gulp, gulp-cli, gulp-sass, gulp-util et gulp-minify-css
- `npm i -D gulp gulp-cli gulp-sass gulp-util gulp-minify-css`

Lancer gulp
- `gulp watch`


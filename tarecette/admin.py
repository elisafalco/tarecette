from django.contrib import admin
from .models import Ingredient, Recipe, Step, Category, IngredientQuantity

admin.site.register(Ingredient)
admin.site.register(IngredientQuantity)
admin.site.register(Category)
admin.site.register(Recipe)
admin.site.register(Step)

"""tarecette URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from .views import index, recipes_list, recipe_detail, categories_list, category_detail

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', index, name="index"),

    path('recettes/', recipes_list, name="recipes_list"),
    path('recettes/<int:recette_id>/<slug:slug>', recipe_detail, name="recipe_detail"),

    path('categories/', categories_list, name="categories_list"),
    path('categories/<int:category_id>/<slug:slug>', category_detail, name="category_detail")
]

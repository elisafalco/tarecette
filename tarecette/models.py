from django.db import models
from django.db.models import CASCADE

from slugify import slugify


class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_recipe_count(self):
        return self.recipes.count()

    def get_slug(self):
        return slugify(self.name)


class Ingredient(models.Model):
    name = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return self.name


class Recipe(models.Model):
    title = models.CharField(max_length=255, unique=True)
    description = models.TextField(blank=True)
    categories = models.ManyToManyField(Category, related_name="recipes", blank=True)
    total_time = models.IntegerField(verbose_name="Temps de préparation", blank=True, null=True, help_text="en minutes")
    created_at = models.DateTimeField(auto_now_add=True)
    edited_at = models.DateTimeField(auto_now=True)
    favorite = models.BooleanField(default=False)
    people_number = models.IntegerField(verbose_name="Nombre de personnes", default=1, null=True, blank=True)

    def __str__(self):
        return self.title

    def get_slug(self):
        return slugify(self.title)


class IngredientQuantity(models.Model):
    ingredient = models.ForeignKey(Ingredient, on_delete=CASCADE)
    recipe = models.ForeignKey(Recipe, on_delete=CASCADE, related_name="quantite_ingredient")
    quantity = models.IntegerField(verbose_name="Quantité", default=1)

    def __str__(self):
        return "%s(x%s) dans %s" % (self.ingredient, self.quantity, self.recipe)


class Step(models.Model):
    description = models.TextField()
    time = models.TimeField(verbose_name="Temps", null=True, blank=True)
    order = models.IntegerField(verbose_name="Ordre de l'étape")
    recipe = models.ForeignKey(Recipe, on_delete=CASCADE, related_name="step")

    def __str__(self):
        return "Etape %s de %s" % (self.order, self.recipe)

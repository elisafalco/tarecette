from django.shortcuts import render

from .models import Recipe, IngredientQuantity, Category


def index(request):

    context = {
        "recipes": Recipe.objects.order_by('-created_at')[0:3],
        "categories": Category.objects.all(),
    }
    return render(request, 'index.html', context)


def recipes_list(request):

    recipes = Recipe.objects.all().order_by("created_at")

    context = {
        "recipes": recipes
    }
    return render(request, 'recipes/list.html', context)


def recipe_detail(request, recette_id, slug):
    recipe = Recipe.objects.get(id=recette_id)
    ingredients = IngredientQuantity.objects.filter(recipe = recipe)

    return render(request, 'recipes/detail.html', {"recipe": recipe,
                                                   "ingredients": ingredients})


def categories_list(request):

    categories = Category.objects.all().order_by('name')

    context = {
        "categories": categories
    }
    return render(request, 'categories/list.html', context)


def category_detail(request, category_id, slug):

    category = Category.objects.get(id=category_id)

    context = {
        "category": category
    }
    return render(request, 'categories/detail.html', context)
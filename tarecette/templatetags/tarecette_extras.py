from django import template

from tarecette.models import Recipe

register = template.Library()

@register.simple_tag
def all_recipes():
    recipes = Recipe.objects.all()
    return recipes